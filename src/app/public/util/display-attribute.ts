import {DisplayAttributeEnum, DisplayAttributeMatchType} from './enums';

export class DisplayAttribute {
  isListPageDisplay: String = 'N';
  isRequired: String = 'N';
  isInsert: String = 'N';
  isDeleteCondition: String = 'N';
  isQueryRequired: String = 'N';
  isLineNew: String = 'N';
  isDetailPageDisplay: String = 'N';
  isAllowUpdate: String = 'N';
  matchType: DisplayAttributeMatchType | string = '';
  displayType: DisplayAttributeEnum | string = '';
  displayName: String = '';
  displayNo: Number = 0;
  validateText: String = '';
  mapFieldColumnCode: String;
}
