export enum SetAssociationTypes {
  filed = 'filed', //属性
  relationship = 'relationship', //关系
  oneToOne = 'oneToOne', //一对一
  oneToMany = 'oneToMany' //一对多
}

export enum EnumCodes {
  displayAttributeEnum = 1007,//控件类型
  displayAttributeMatchType = 1008,//匹配方式
}

//'Autocomplete 自动完成, Cascader 级联选择 , DatePicker 日期选择框 , TimePicker 时间选择框 ,
// Input 单行文本框 , Textarea 多行文本框 , InputNumber 数字输入框 , Mobile 手机 ,
// Phone 固话 , MobileOrPhone 手机或固话 ,Password 密码 , Email 邮箱 , Website 网址 , IdCard 身份证 , Mention 提及(@) ,
// Select 单项选择器 , MultiSelect 多项选择器 , Radio 单选按钮 , Checkbox 复选框 ,
// Rate 评分 , Silder 滑动条 ,Switch 开关按钮 , TreeSelect 选择树 , Upload 上传 ',
export enum DisplayAttributeEnum {
  autocomplete = 'Autocomplete', //自动完成
  cascader = 'Cascader', //级联选择
  datePicker = 'DatePicker', //日期选择框
  timePicker = 'TimePicker', //时间选择框
  input = 'Input', //单行文本框
  inputNumber = 'InputNumber', //数字输入框
  textarea = 'Textarea', //多行文本框
  mobile = 'Mobile', //手机
  phone = 'Phone', //固话
  mobileOrPhone = 'MobileOrPhone', //手机或固话
  password = 'Password', //密码
  email = 'Email', //邮箱
  website = 'Website', //网址
  idCard = 'IdCard', //身份证
  mention = 'Mention', //提及(@)
  select = 'Select', //单项选择器
  multiSelect = 'MultiSelect', //多项选择器
  radio = 'Radio', //单选按钮
  checkbox = 'Checkbox', //复选框
  rate = 'Rate', //评分
  silder = 'Silder', //滑动条
  switch = 'Switch', //开关按钮
  treeSelect = 'TreeSelect', //选择树
  upload = 'Upload', //上传
}

export enum DisplayAttributeMatchType {
  LefLike = 'leftLike',
  LessOrEqual = '<=',
  NotEqual = '!=',
  Like = 'like',
  RightLike = 'rightLike',
  Equal = '=',
  In = 'in',
  ThanOrEqual = '>=',
  Than = '>',
  Between = 'between',
  Less = '<'
}
