import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {PublicModule} from './public/public.module';
import {RoutesModule} from './routes/routes.module';
import {SharedModule} from './shared/shared.module';

import {AppComponent} from './app.component';
import {MainComponent} from './layout/main/main.component';
import {SimpleComponent} from './layout/simple/simple.component';
import {PageComponent} from './layout/page/page.component';
import {NZ_I18N, zh_CN} from 'ng-zorro-antd';
import {HttpClientModule} from '@angular/common/http';
import {registerLocaleData} from '@angular/common';
import zh from '@angular/common/locales/zh';

registerLocaleData(zh);

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    SimpleComponent,
    PageComponent
  ],
  imports: [
    BrowserAnimationsModule,//浏览器动画模块
    BrowserModule, //浏览器支持
    PublicModule, // 核心模块，该模块注入了项目必须的服务
    RoutesModule, // 路由模块
    SharedModule.forRoot(),
    HttpClientModule // 公用模块
  ],
  providers: [{provide: NZ_I18N, useValue: zh_CN}],
  bootstrap: [AppComponent]
})
export class AppModule {
}
