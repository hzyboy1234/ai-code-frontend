import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Setting} from '../../../public/setting/setting';
import {Router} from '@angular/router';
import {NzNotificationService} from 'ng-zorro-antd';
import {LoginService} from '../login.service';
import {SettingUrl} from '../../../public/setting/setting_url';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  array = ['../../../../assets/img/1.jpg'];//广告banner
  validateForm: FormGroup;//登录的表单
  app = Setting.APP; //平台基本信息
  registerUrl: string = SettingUrl.ROUTERLINK.login.reg; //注册页面路由

  constructor(public fb: FormBuilder, public router: Router, public _notification: NzNotificationService, public loginService: LoginService) {
    this.validateForm = this.fb.group({
      account: [null, [Validators.required]],
      pwd: [null, [Validators.required]]
    });
  }

  ngOnInit() {
  }

  /**
   * 登录
   * @param $event
   * @param value
   */
  login = () => {
    let me = this;
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
    }
    if (this.validateForm.invalid) return;
    this.loginService.login(this.validateForm.value.account, this.validateForm.value.pwd).then((res:any) => {
      if (res.success) {
        me._notification.success('成功', '登录成功！');
        sessionStorage.setItem('token', res.data); //设置token
        this.router.navigate([SettingUrl.ROUTERLINK.project.list]); //路由跳转（首页）
      }
    });
  };

  /**
   * 获取每个输入框的状态
   * @param name
   * @returns {AbstractControl}
   */
  getFormControl(name) {
    return this.validateForm.controls[name];
  }


}
