import {Injectable} from '@angular/core';
import {AjaxService} from '../../public/service/ajax.service';
import {SettingUrl} from '../../public/setting/setting_url';
import {NzNotificationService} from 'ng-zorro-antd';
import {Router} from '@angular/router';

@Injectable()
export class LoginService {

  constructor(public _notification: NzNotificationService, private ajaxService:AjaxService) {
  }

  /**
   * 用户登录
   * @param account
   * @param pwd
   * @returns {{success: boolean; info: string}}
   */
  login(account, pwd) {
    let me = this;
    return new Promise(function (resolve) {
      me.ajaxService.get({
        url: SettingUrl.URL.login.signin,
        data: {account: account, password: pwd},
        success: (res) => {
          if(res.success) resolve(res);
          else me._notification.error('警告', res.info)
        }
      });
    })
  }


  /**
   * 用户注册
   * @param account
   * @param pwd
   * @returns {{success: boolean; info: string}}
   */
  reg = (account, pwd) => {
    let ret = {success: false, info: '注册失败！'};
    this.ajaxService.post({
      url: SettingUrl.URL.login.reg,
      data: {account: account, password: pwd},
      async: false,
      success: (res) => {
        ret = res;
      }
    });
    return ret;
  };

}
