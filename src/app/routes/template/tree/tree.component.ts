import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.css']
})
export class TreeComponent implements OnInit {
  @Input() data: any;
  @Input() curChecked: string;
  @Output() changeChecked = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  changeCheckedFile(data) {
    this.changeChecked.emit({
      curChecked: data.title
    })
  }
}
