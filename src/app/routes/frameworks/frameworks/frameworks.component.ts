import {Component, OnInit} from '@angular/core';
import {Page} from '../../../public/util/page';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PatternService} from '../../../public/service/pattern.service';

import * as $ from 'jquery';
import {FrameworksService} from '../frameworks.service';

@Component({
  selector: 'app-frameworks',
  templateUrl: './frameworks.component.html',
  styleUrls: ['./frameworks.component.css']
})
export class FrameworksComponent implements OnInit {
  framesList: Page = new Page();
  isVisible: boolean = false;
  isConfirmLoading = false;
  validateForm: FormGroup;//企业登录的表单
  curCode: string;//修改的数据编码

  constructor(private frameworksService: FrameworksService, private fb: FormBuilder,) {
    this.validateForm = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.maxLength(50)])],//名称
      account: [null, [Validators.required]],//模板仓库账户
      password: [null, [Validators.required]],//模板仓库密码
      gitHome: [null, Validators.compose([Validators.required, Validators.pattern(PatternService.website)])],//模板仓库地址
      isPublic: [null],//是否是公开库
      description: [null, [Validators.required]],//技术描述
    });
  }

  ngOnInit() {
    this.getFramesList();//获取技术列表
  }

  addFrames() {
    this.isVisible = true;
    this.curCode = null;
    this.validateForm = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.maxLength(50)])],//名称
      account: [null, [Validators.required]],//模板仓库账户
      password: [null, [Validators.required]],//模板仓库密码
      gitHome: [null, Validators.compose([Validators.required, Validators.pattern(PatternService.website)])],//模板仓库地址
      isPublic: [null],//是否是公开库
      description: [null, [Validators.required]],//技术描述
    });
  }

  /**
   * 修改技术
   * @param code
   */
  modifyFrames(code) {
    this.isVisible = true;
    this.curCode = code;
    this.validateForm = this.fb.group({
      name: [null, Validators.compose([Validators.required, Validators.maxLength(50)])],//名称
      isPublic: [null],//是否是公开库
      description: [null, [Validators.required]],//技术描述
    });
    this.loadFrames();
  }

  /**
   * 获取技术列表
   */
  getFramesList(curPage?) {
    let me = this;
    if (curPage) me.framesList.curPage = curPage;
    let params = {
      curPage: me.framesList.curPage,
      pageSize: 100
    };
    $.when(me.frameworksService.framesList(params)).done(res => {
      this.framesList = res;
    })
  }

  /**
   * 获取技术详情
   */
  loadFrames() {
    let me = this;
    $.when(me.frameworksService.loadFrameworkss({code: this.curCode})).done(res => {
      if (res) {
        res.isPublic = res.isPublic === 'Y';
        me.validateForm.patchValue(res);
      }
    })
  }

  /**
   * 弹窗确定按钮
   * @param e
   */
  handleOk = (e) => {
    let me = this;
    //1.进行脏检查，提示未填的必填字段
    for (const key in me.validateForm.controls) {
      me.validateForm.controls[key].markAsDirty();
      me.validateForm.controls[key].updateValueAndValidity();
    }
    if (me.validateForm.invalid) return;
    me.isConfirmLoading = true;
    let formData = Object.assign({}, me.validateForm.value);
    formData.isPublic = formData.isPublic ? 'Y' : 'N';
    if (!me.curCode) {
      $.when(me.frameworksService.buildFrames(formData)).done(data => {
        me.isConfirmLoading = false;
        if (data) {
          me.validateForm.reset();
          me.isVisible = false;
          this.getFramesList(1);//添加成功要刷新第一页
        }
      });
    } else {
      formData.code = this.curCode;
      $.when(me.frameworksService.modifyFrameworks(formData)).done(data => {
        me.isConfirmLoading = false;
        if (data) {
          me.handleCancel();
          this.getFramesList();//添加成功要刷新当前页
        }
      });
    }
  };

  /**
   * 弹窗取消按钮
   */
  handleCancel() {
    this.isVisible = false;
    this.validateForm.reset();
  };

  /**
   * 删除技术架构
   * @param id
   */
  delFrame(code) {
    let me = this;
    $.when(me.frameworksService.deleteFrameworks({code: code})).done(data => {
      if (data) {
        this.getFramesList();//添加成功要刷新
      }
    });
  }

  /**
   * 修改技术架构状态
   * @param id
   */
  modifyFrameworksState(state, code) {
    let me = this;
    let isPublic = state ? 'Y' : 'N';
    $.when(me.frameworksService.modifyFrameworksState({isPublic: isPublic, code: code})).done(data => {
      if (!data) {
        this.getFramesList();//修改失败要刷新
      }
    });
  }

  /**
   * 获取每个输入框的状态
   * @param name
   * @returns {AbstractControl}
   */
  getFormControl(name) {
    return this.validateForm.controls[name];
  }

}
