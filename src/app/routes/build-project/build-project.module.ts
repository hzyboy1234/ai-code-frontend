import {NgModule} from '@angular/core';
import {ProjectStepsComponent} from './project-steps/project-steps.component';
import {ProjectInfoComponent} from './project-info/project-info.component';
import {ProjectFrameComponent} from './project-frame/project-frame.component';
import {ProjectRepositoryComponent} from './project-repository/project-repository.component';
import {ProjectSqlComponent} from './project-sql/project-sql.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {BuildProjectService} from './build-project.service';
import {ProjectDatabaseComponent} from './project-database/project-database.component';
import {ProjectDatabaseAssociationComponent} from './project-database-association/project-database-association.component';
import {ProjectDatabaseFiledsComponent} from './project-database-fileds/project-database-fileds.component';

const routes: Routes = [
  {path: '', redirectTo: 'proSteps'},
  {
    path: 'proSteps', component: ProjectStepsComponent, children: [
      {path: '', redirectTo: 'proInfo'},
      {path: 'proInfo', component: ProjectInfoComponent},
      {path: 'proFrames', component: ProjectFrameComponent},
      {path: 'proRepository', component: ProjectRepositoryComponent},
      {path: 'proDatabase', component: ProjectDatabaseComponent},
      {path: 'proSql', component: ProjectSqlComponent}
    ]
  }
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProjectStepsComponent,
    ProjectInfoComponent,
    ProjectFrameComponent,
    ProjectRepositoryComponent,
    ProjectSqlComponent,
    ProjectDatabaseComponent,
    ProjectDatabaseAssociationComponent,
    ProjectDatabaseFiledsComponent],
  providers: [BuildProjectService]
})
export class BuildProjectModule {
}
