import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectDatabaseFiledsComponent } from './project-database-fileds.component';

describe('ProjectDatabaseFiledsComponent', () => {
  let component: ProjectDatabaseFiledsComponent;
  let fixture: ComponentFixture<ProjectDatabaseFiledsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectDatabaseFiledsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectDatabaseFiledsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
