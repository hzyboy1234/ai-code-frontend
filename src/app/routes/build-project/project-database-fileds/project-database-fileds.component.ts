import {Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges} from '@angular/core';
import {MainService} from '../../../public/service/main.service';
import {DisplayAttributeEnum, EnumCodes} from '../../../public/util/enums';
import {BuildProjectService} from '../build-project.service';
import {DisplayAttribute} from '../../../public/util/display-attribute';

@Component({
  selector: 'app-project-database-fileds',
  templateUrl: './project-database-fileds.component.html',
  styleUrls: ['./project-database-fileds.component.scss']
})
export class ProjectDatabaseFiledsComponent implements OnInit, OnChanges, OnDestroy {
  @Input() curTableCode: string;
  @Input() tableName: string;
  public matchType: string;
  public displayType: string;
  public displayAttributes: Array<any> = [];
  public matchTypes: Array<any> = [];
  public filedsData: Array<any> = [];
  public isLoading: boolean = false;
  public allChecked: any = {};//选中所有
  public myIndeterminates: any = {};//部分选中

  constructor(private buildProjectService: BuildProjectService,private mainService: MainService) {
    this.displayAttributes = this.mainService.getEnumDataList(EnumCodes.displayAttributeEnum);
    this.matchTypes = this.mainService.getEnumDataList(EnumCodes.displayAttributeMatchType);
    this.displayAttributes.unshift({key: '', val: '[无]'});
    this.matchTypes.unshift({key: '', val: '[无]'});
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes && changes['curTableCode'] && this.curTableCode) this.getFieldsList();
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    // this.saveFiledsAttributes();
  }

  /**
   * 获取排序
   */
  getSort() {
    let showableData = this.filedsData.filter(item => item.displayAttribute.isListPageDisplay);
    this.filedsData = this.filedsData.map(item => {
      showableData.map((it, index) => {
        it.displayAttribute.displayNo = it.displayAttribute.isListPageDisplay ? index + 1 : 0;
        if (item.code === it.code) item = it;
      });
      return item;
    });
  }

  public getFieldsList() {
    this.isLoading = true;
    let data = {
      mapClassTableCode: this.curTableCode
    };
    this.allChecked = {};
    this.myIndeterminates = {};
    this.buildProjectService.getFieldsList(data).then((list: Array<any>) => {
      this.isLoading = false;
      if (list) {
        this.filedsData = list.map((item, idx) => {
          item.isPrimaryKey = item.isPrimaryKey === 'Y';
          item.isDate = item.isDate === 'Y';
          item.isState = item.isState === 'Y';
          if (!item.displayAttribute) item.displayAttribute = new DisplayAttribute();
          item.displayAttribute = {
            isListPageDisplay: item.displayAttribute.isListPageDisplay === 'Y',
            isRequired: item.displayAttribute.isRequired === 'Y',
            isInsert: item.displayAttribute.isInsert === 'Y',
            isDeleteCondition: item.displayAttribute.isDeleteCondition === 'Y',
            isQueryRequired: item.displayAttribute.isQueryRequired === 'Y',
            isLineNew: item.displayAttribute.isLineNew === 'Y',
            isDetailPageDisplay: item.displayAttribute.isDetailPageDisplay === 'Y',
            isAllowUpdate: item.displayAttribute.isAllowUpdate === 'Y',
            matchType: item.displayAttribute.matchType || '',
            displayType: item.displayAttribute.displayType || '',
            displayName: item.displayAttribute.displayName || item.notes.substring(0, 6),
            displayNo: item.displayAttribute.displayNo || (idx + 1),
            validateText: item.displayAttribute.validateText || `请输入${item.notes.substring(0, 6)}`,
            mapFieldColumnCode: item.code
          };
          if (item.isDate) item.displayAttribute.matchType = DisplayAttributeEnum.datePicker;
          if (item.isState) item.displayAttribute.matchType = DisplayAttributeEnum.select;
          return item;
        });
        ['isListPageDisplay', 'isRequired', 'isInsert', 'isDeleteCondition', 'isQueryRequired', 'isLineNew', 'isDetailPageDisplay', 'isAllowUpdate'].forEach(item => {
          this.refreshStatus(item);
        });
      }
    }).catch(err => {
      this.isLoading = false;
    });
  }

  /**
   * 保存
   */
  saveFiledsAttributes() {
    let displayAttributes = this.filedsData.map(item => {
      return {
        isListPageDisplay: item.displayAttribute.isListPageDisplay ? 'Y' : 'N',
        isRequired: item.displayAttribute.isRequired ? 'Y' : 'N',
        isInsert: item.displayAttribute.isInsert ? 'Y' : 'N',
        isDeleteCondition: item.displayAttribute.isDeleteCondition ? 'Y' : 'N',
        isQueryRequired: item.displayAttribute.isQueryRequired ? 'Y' : 'N',
        isLineNew: item.displayAttribute.isLineNew ? 'Y' : 'N',
        isDetailPageDisplay: item.displayAttribute.isDetailPageDisplay ? 'Y' : 'N',
        isAllowUpdate: item.displayAttribute.isAllowUpdate ? 'Y' : 'N',
        matchType: item.displayAttribute.matchType || '',
        displayType: item.displayAttribute.displayType || '',
        displayName: item.displayAttribute.displayName || '',
        displayNo: item.displayAttribute.displayNo || 0,
        validateText: item.displayAttribute.validateText || '',
        mapFieldColumnCode: item.code
      };
    });
    this.buildProjectService.fieldsSave(displayAttributes);
  }

  /**
   * 选中所有
   * @param value
   */
  checkAll(value: boolean, filed): void {
    this.filedsData.forEach(item => {
      item.displayAttribute[filed] = value;
    });
  }

  /**
   * 更新状态
   * @param data
   */
  refreshStatus(filed): void {
    let allChecked = this.filedsData.every(value => value.displayAttribute[filed] === true);
    let allUnChecked = this.filedsData.every(value => !value.displayAttribute[filed]);
    this.allChecked[filed] = allChecked;
    this.myIndeterminates[filed] = (!allChecked) && (!allUnChecked);
  }
}
