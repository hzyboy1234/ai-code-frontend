import {SimpleComponent} from '../layout/simple/simple.component';
import {PageComponent} from '../layout/page/page.component';

export const routes = [
  {
    path: 'main',
    component: SimpleComponent,
    children: [
      {path: '', redirectTo: '/main/project', pathMatch: 'full'},
      {path: 'home', loadChildren: './home/home.module#HomeModule'},
      {path: 'project', loadChildren: './project/project.module#ProjectModule'},
      {path: 'frameworks', loadChildren: './frameworks/frameworks.module#FrameworksModule'},
      {path: 'buildPro', loadChildren: './build-project/build-project.module#BuildProjectModule'},
      {path: 'template', loadChildren: './template/template.module#TemplateModule'}
    ]
  },
  {
    path: 'login',
    component: PageComponent,
    children: [
      {path: '', redirectTo: '/login/index', pathMatch: 'full'},
      {path: 'index', loadChildren: './login/login.module#LoginModule'}
    ]
  },
  // 路由指向找不到时，指向这里
  {path: '**', redirectTo: '/login/index'}
];
