import {Component, OnInit} from '@angular/core';
import {ProjectService} from '../project.service';
import {ActivatedRoute, Router} from '@angular/router';
import * as $ from 'jquery';
import {SettingUrl} from '../../../public/setting/setting_url';
import {WebsocketService} from '../../../public/service/websocket.service';

@Component({
  selector: 'app-log-page',
  templateUrl: './log-page.component.html',
  styleUrls: ['./log-page.component.css']
})
export class LogPageComponent implements OnInit {
  public logInfo: string = '';          //日志信息
  public code: string;                      //项目编码
  public time: string;                      //项目构建时间
  public home: any;                         //仓库的地址,当检测到日志打印完之后跳转到仓库的地址
  public state: any;                        //构建历史状态
  public websocketUrl = SettingUrl.URL.base.websocket;//本地的websocket地址

  constructor(public projectService: ProjectService,
              public router: Router,
              public routerInfo: ActivatedRoute,
              private wsService: WebsocketService) {
  }

  ngOnInit() {
    let me = this;
    me.code = me.routerInfo.snapshot.queryParams['code'];
    me.time = me.routerInfo.snapshot.queryParams['time'];
    me.home = me.routerInfo.snapshot.queryParams['home'];
    me.state = me.routerInfo.snapshot.queryParams['state'];
    if (me.time) {
      me.getHistoryLog();//如果是查看历史日志，获取历史日志
    } else {
      this.wsService.closeWs();//如果不是查看历史日志，尝试关闭socket，稍后重新连接
      setTimeout(_ => {
        this.websocket();
      },300);
    }
  }

  websocket() {
    this.wsService
      .createObservableSocket(this.websocketUrl)
      .subscribe(data => {
          if (data.open) {
            setTimeout(_ => {
              this.getLog()
            },500);
          } else if (JSON.stringify(data).indexOf('End') != -1) {
            this.wsService.closeWs();
          } else if(!this.state || (this.state && this.state === 'Executing')) { //状态不存在（构建）或者是进行中的状态加载日志
            this.logInfo += data + '\n';
            LogPageComponent.scrollBottom();
          }
        },
        err => console.log(err),
        () => console.log('流已经结束')
      );
  }

  /**
   * 通过使页面底部的一个元素实施滚动到页面可见部分来达到页面跟随日志滚动的目的
   */
  static scrollBottom() {
    let logBottom = document.getElementById('log-bottom');
    logBottom.scrollIntoView();
  }

  /**
   * 执行任务生成任务编码，如果构建历史跳转过来的话,就有任务编码直接调用打印日志的方法
   */

  getLog() {
    let data = {
      code: this.code
    };
    this.projectService.excuteTask(data).catch(err => {
      this.wsService.closeWs();
    });
  }

  /**
   * 请求构建历史日志
   */
  getHistoryLog() {
    let me = this;
    let data = {
      datetime: me.time,
      projectCode: me.code,//任务编码
    };
    $.when(me.projectService.getLogsList(data)).done(data => {
      this.logInfo = data;
      //当websocket处于连接状态并且是此次构建未完成时，继续观察websocket
      if (me.wsService.ws && (me.state == 'Create' || me.state == 'Executing')) {
        me.websocket();
      }
    });
  }
}
