
## create

Create by `angular-cli` and based on
[Ant Design of Angular](https://ng.ant.design/docs/introduce/en) (also
named `NG-ZORRO`) UI framework

## usage

```
$ npm install
$ npm start
```

## build

```
$ npm run build
```

but we recommend the following command:

```
$ npm run build:prod
```

If app is already built, to run built version use:

```
$ npm run node:server
```

module.exports = PROXY_CONFIG;
```

It was configed in `package.json`, also the file name can be changed as you like:

```
"start": "ng serve --proxy-config proxy.conf.js"
```

## request

We use ajax to request data, and we have suffix property to determine whether to suffix the URL.
